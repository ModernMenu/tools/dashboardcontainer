FROM python:3.9.10-bullseye

RUN apt-get update \
    && apt-get -y install libpq-dev gcc git \
    && pip install psycopg2

WORKDIR /app
RUN git clone https://gitlab.com/ModernMenu/dashboard.git .
# RUN sed -i "s/localhost/${DOCKER_HOST}/g" /app/app.py
RUN sed -i "s/localhost/pg_container/g" /app/app.py

# ARG expr1='re.sub("5000/qrcode", "5001/table"+str(table+1), url)'
# ARG expr2='url.replace("admin-","").replace("qrcode", numtable)'
# RUN sed -i "s@$expr1@$expr2@" /app/controller/gestionController.py

RUN pip install -r requirements.txt

RUN export FLASK_APP=app.py && export FLASK_ENV=development

ENTRYPOINT [ "bash", "-l", "-c" ]
CMD [ "python3 app.py" ]
